﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMSOFT.EasyHotel.Admin.Models
{
    public class RoomModel
    {
        public int RoomId { get; set; }
        public string RoomNo { get; set; }
        public int RoomTypeId { get; set; }
        public string RoomTypeName { get; set; }
        public int SubHotelId { get; set; }
        public bool IsMending { get; set; }
        public string SubHotelName { get; set; }
    }
}