﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation.MVC;
namespace IEMSOFT.EasyHotel.Admin.Controllers
{
    public class RoomTypeController : BaseController
    {
        //
        // GET: /RoomType/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Get()
        {
            using (var db = new EasyHotelDB())
            {
                var dal = new RoomTypeDAL(db);
                var roomTypes = dal.GetList(CurrentUser.GroupHotelId);
                var model = AutoMapper.Mapper.Map<List<RoomTypeModel>>(roomTypes);
                return JsonNet(model);
            }
        }

        public ActionResult GetOne(int roomId)
        {
            var ret = new BasicResult<RoomTypeModel, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var roomDal = new RoomDAL(db);
                var roomData = roomDal.GetOne(roomId);
                var roomTypeDAL = new RoomTypeDAL(db);
                var roomTypeData = roomTypeDAL.GetOne(roomData.RoomTypeId);
                ret.Data = AutoMapper.Mapper.Map<RoomTypeModel>(roomTypeData);
                return JsonNet(ret);
            }
        }

        public ActionResult Option(string defaultText, string defaultValue)
        {
            List<SelectListItem> options;
            using (var db = new EasyHotelDB())
            {
                var dal = new RoomTypeDAL(db);
                var ret = dal.GetList(CurrentUser.GroupHotelId);
                options = ret.Select((item) => new SelectListItem
                {
                    Text = item.Name,
                    Value = item.RoomTypeId.ToString()
                }).ToList();
                if (!string.IsNullOrEmpty(defaultText))
                {
                    options.Insert(0, new SelectListItem { Text = defaultText, Value = defaultValue ?? "", Selected = true });
                }
                return JsonNet(options);
            }
        }

        [HttpPost]
        public ActionResult Save(RoomTypeModel model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB())
            {
                var dal = new RoomTypeDAL(db);
                var dalModel = AutoMapper.Mapper.Map<RoomType>(model);
                dalModel.GroupHotelId = CurrentUser.GroupHotelId;
                dal.Save(dalModel);
            }
            return JsonNet(ret);
        }

        [HttpPost]
        public ActionResult Remove([FromJson]List<RoomTypeModel> model)
        {
            var ret = new BasicResult<string, List<string>>();
            ret.Msg = new List<string>();
            using (var db = new EasyHotelDB(true))
            {
                using (var tx = db.DB.GetTransaction())
                {
                    var dal = new RoomTypeDAL(db);
                    var dalModel = AutoMapper.Mapper.Map<List<RoomType>>(model);
                    dal.Remove(dalModel);
                    tx.Complete();
                }
            }
            return JsonNet(ret);
        }
    }
}
