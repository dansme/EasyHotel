﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IEMSOFT.EasyHotel.DAL;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Admin.Lib;
namespace IEMSOFT.EasyHotel.Tests
{
    [TestClass]
    public class DALTests
    {
        [TestMethod]
        public void RoleDALTest()
        {
            using (var db = new EasyHotelDB(true))
            {
                var billDal = new BillDAL(db);
                AutomapperConfig.Init();
                var ret = billDal.GetPendingBills(DateTime.Now, 2);
                var ret1 = BillBLL.PreSettle(2, DateTime.Now, 0);
                var ret2 = BillBLL.PreSettle(2, new DateTime(2014,9,15), 0);
                var ret3 = BillBLL.PreSettle(2, new DateTime(2014, 9, 15,14,0,0), 0);
                var ret4 = BillBLL.PreSettle(2, new DateTime(2014, 9, 15, 15, 0, 0), 0);
                var ret5 = BillBLL.PreSettle(2, new DateTime(2014, 9, 15, 11, 0, 0), 0);

            }
        }

        [TestMethod]
        public void GroupHotelDALTest()
        {
            AutomapperConfig.Init();
            var date = new DateTime(2014, 9, 12);
         var ret=   RoomBLL.GetRoomStatus(date,2);
        }
    }
}
